#include <iostream>
#include <conio.h>
#include <string>


using namespace std;

//function prototype
void PrintSomething();
int AddInts(int n1, int n2);
int AddInts(int n1, int n2, int &n3);

int Fact(int num)
{
	if (num == 0)
	{
		return 1;
	}
	
	return num * Fact(num - 1);
}

//Function to ask user if they want to run again.

bool Confirm(string msg)
{
	char input = 'n';
	cout << msg;
	cin >> input;

	if (input == 'y' || input == 'Y') return true;
	if (input == 'n' || input == 'N') return true;

	//Asks Again

	return Confirm(msg);
}


int main()
{

	// cout << "Hi";
	// main(); //recursive -- causes stack overflow

	char quit = 'n';

	//Loops the function over again

	do
	{
		cout << Fact(5) << endl;
		//cout << "Show Again? (y/n): ";
		//cin >> quit;
	} while (Confirm("Would you like to continue? (y/n): "));

	

	PrintSomething();

	int i = 4;
	int j = 2;
	int k = 36;
	cout << AddInts(i, j, k);
	
	_getch();
	return 0;
}

void PrintSomething()
{
	cout << "Something";
}

int AddInts(int n1, int n2)
{
	return n1 + n2;
}

// Overload the add ints function
int AddInts(int n1, int n2, int &n3)
{
	n1++;
	n2++;
	n3++;

	return n1 + n2 + n3;
}